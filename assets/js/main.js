$('.video-btn').on('click', function(){
    $(this).toggleClass('faded');
    $(".video iframe")[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
});

$('.add-review-btn').on("click", function(){
    if($('.add-review-modal').hasClass('active')){
        $('.add-review-modal').removeClass('active');
    }
    else{
        $('.add-review-modal').addClass('active');
    }
});

$('.close-btn').on('click', function(){
    $('.add-review-modal').removeClass('active');
})